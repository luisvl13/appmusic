const URL = 'https://ws.audioscrobbler.com/2.0/?method=geo.gettopartists&country=spain&api_key=c4a23f381f28eb8c362142e622c03433&format=json';

function getArtists(){
    return fetch(URL)
        .then(response => response.json())
        .then(data => data.topartists.artist)
        .then(artists => artists.map(artist => {
            return{
                name: artist.name,
                image: artist.image[3]['#text'],
                likes: 200,
                comments: 15
            }
        }))
}

export { getArtists }
