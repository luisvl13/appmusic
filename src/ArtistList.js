
import React, { Component } from 'react';
import {
  StyleSheet,
  ListView,
  TouchableOpacity,
} from 'react-native';
//Import terceros
import {Actions} from 'react-native-router-flux';

//
import ArtistBox from './ArtistBox';

export default class ArtistList extends Component<{}> {
    constructor(props) { //primera vez que se instancia y despues ya nos e llama el contrcutor
      super(props);
      const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

      this.state = {
            dataSource: ds
      }
    }

    componentDidMount(){
        this.updateDataSource( this.props.artists );
    }

    componentWillReceiveProps(newProps){//Se llama cada vez que cambian las poriedades que se le van a pasar al componente
        if( newProps.artists !== this.props.artists ){
            this.updateDataSource( newProps.artists );
        }
    }

    updateDataSource = data => {
        this.setState({
            dataSource: this.state.dataSource.cloneWithRows( data )
        })
    }

    handlePress( artist ){
        Actions.artistDetail({ artist: artist })
    }

    render() {
    return (
        <ListView
          enableEmptySections={true}
          dataSource={this.state.dataSource}
          renderRow={( artist ) => {
              return (
                  <TouchableOpacity
                      onPress={ () => this.handlePress( artist ) }>
                      <ArtistBox artist={artist} />
                  </TouchableOpacity>
              )
          }}
        />
    );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'lightgray',
    paddingTop: 50
  },
});
